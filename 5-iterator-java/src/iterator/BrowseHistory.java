package iterator;

// import java.util.ArrayList;
// import java.util.List;

public class BrowseHistory {
    // private List<String> _urls = new ArrayList<>(); /* List */
    private final String[] _urls = new String[10];
    private int _count;

    public void push(final String url) {
        // _urls.add(url); /* List */
        _urls[_count++] = url;
    }

    public String pop() {
        // var lastIndex = _urls.size() -1; /* List */
        // var lastUrl = _urls.get(lastIndex);
        // _urls.remove(lastIndex);
        // return lastUrl;

        return _urls[--_count];
    }

    public IBaseIterator createListIterator() {
        // return new ListIterator(this); /* List */
        return new ArrayIterator(this);
    }

    // public class ListIterator implements IBaseIterator{
    // private BrowseHistory _history;
    // private Integer _index = 0;

    // public ListIterator(BrowseHistory history){
    // _history = history;
    // }

    // @Override
    // public String current() {
    // return _history._urls.get(_index);
    // }

    // @Override
    // public void next() {
    // _index++;
    // }

    // @Override
    // public boolean isDone() {
    // return (_index < _history._urls.size());
    // }

    // }

    public class ArrayIterator implements IBaseIterator {
        private BrowseHistory _history;
        private int _index;

        public ArrayIterator(BrowseHistory history) {
            _history = history;
        }

        @Override
        public String current() {
            return _history._urls[_index];
        }

        @Override
        public void next() {
            _index++;

        }

        @Override
        public boolean isDone() {
            return (_index < _history._count);
        }
    }
}
