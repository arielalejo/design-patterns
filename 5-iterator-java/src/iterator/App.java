package iterator;

public class App {
    public static void main(String[] args) {
        var history = new BrowseHistory();
        history.push("1");
        history.push("2");
        history.push("3");
        history.push("4");
        history.pop();

        var listIterator = history.createListIterator();
        while (listIterator.isDone()) {
            var url = listIterator.current();
            System.out.println(url);
            listIterator.next();
        }
    }
}
