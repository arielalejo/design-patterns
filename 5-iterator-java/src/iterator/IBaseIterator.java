package iterator;

public interface IBaseIterator {
    String current();
    void next();
    boolean isDone();
}