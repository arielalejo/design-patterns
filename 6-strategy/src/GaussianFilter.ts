import { IFilter } from './IFilter';

export class GaussianFilter implements IFilter {
    apply(fileName: string): void {
        console.log(`${fileName} filtered with Gaussian filter`);
    }
}
