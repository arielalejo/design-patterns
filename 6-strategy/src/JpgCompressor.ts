import { ICompressor } from "./ICompressor";

export class JpgCompressor implements ICompressor{
    compress(fileName: string): void {
        console.log(`${fileName} compressed with JPG`);
    }
}