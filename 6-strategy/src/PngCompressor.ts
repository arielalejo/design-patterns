import { ICompressor } from "./ICompressor";

export class PngCompressor implements ICompressor{
    compress(fileName: string): void {
        console.log(`${fileName} compressed with PNG`);
    }

}