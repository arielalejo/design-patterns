import { ImageStorage } from "./ImageStorage";
import { JpgCompressor } from "./JpgCompressor";
import { GaussianFilter } from "./GaussianFilter";
import { PngCompressor } from "./PngCompressor";

let imgStorage = new ImageStorage();
imgStorage.store("/photo.png", new JpgCompressor(), new GaussianFilter());
imgStorage.store("/landscape.png", new PngCompressor(), new GaussianFilter());
