import { IFilter } from './IFilter';
import { ICompressor } from './ICompressor';

export class ImageStorage {
    /* for creating ImageStorage objects with a specific compresor and filter  */
    // private _compresor: ICompressor;
    // private _filter: IFilter;
    // private _imagePath :string;

    // constructor(compresor: ICompresor, filter: IFilter){
    //     this._compresor = compresor;
    //     this._filter = filter
    // }

    // store(fileName: string){
    //     this._compresor.compress(fileName);
    //     this._filter.apply(fileName)
    // })

    /* other approach: ImageStorage objects are generic -> every time we
    use the 'store' method we define the type of compresor and filter  */
    store(fileName: string, compressor: ICompressor, filter: IFilter) {
        compressor.compress(fileName);
        filter.apply(fileName);
    }
}
