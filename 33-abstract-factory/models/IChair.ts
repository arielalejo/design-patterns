export interface IChair {
    hasLegs(): void;
    sitOn(): void;
}
