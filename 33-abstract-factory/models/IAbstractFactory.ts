import { IChair } from "./IChair";
import { ISofa } from './ISofa';

export interface IAbstractFactor{
    createChair(): IChair,
    createSofa(): ISofa
}