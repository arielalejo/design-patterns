import { IAbstractFactor } from "./models/IAbstractFactory";
import { IChair } from "./models/IChair";
import { ISofa } from "./models/ISofa";
import { ModernChair } from "./ModernChair";
import { ModernSofa } from "./ModernSofa";

export class ModernFurnitureFactory implements IAbstractFactor{
    createChair(): IChair {
        return new ModernChair();
    }
    createSofa(): ISofa {
        return new ModernSofa();
    }

}