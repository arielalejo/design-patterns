import { IAbstractFactor } from "./models/IAbstractFactory";
import { IChair } from "./models/IChair";
import { ISofa } from "./models/ISofa";
import { VictorianChair } from "./VictorianChair";
import { VictorianSofa } from "./VictorianSofa";

export class VictorianFurnitureFactory implements IAbstractFactor{
    createChair(): IChair {
        return new VictorianChair()
    }
    createSofa(): ISofa {
        return new VictorianSofa()
    }
}