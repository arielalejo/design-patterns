import { VictorianFurnitureFactory } from "./VictorianFurnitureFactory";
import { ModernFurnitureFactory } from './ModernFurnitureFactory';

const victorianFactory = new VictorianFurnitureFactory();
const victChair1 = victorianFactory.createChair();
const victChair2 = victorianFactory.createChair();

const modernFactory = new ModernFurnitureFactory();
const moderSofa1 = modernFactory.createSofa();
