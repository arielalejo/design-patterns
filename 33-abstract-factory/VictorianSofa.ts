import { ISofa } from './models/ISofa';

export class VictorianSofa implements ISofa {
    hasCojin(): void {
        console.log('victorian sofa cojin');
    }
    sitOn(): void {
        console.log('victorian sofa sitOn');
    }
}
