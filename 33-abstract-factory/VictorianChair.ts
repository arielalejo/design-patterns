import { IChair } from "./models/IChair";

export class VictorianChair implements IChair{
    hasLegs(): void {
        console.log("victorian chair legs")
    }
    sitOn(): void {
        console.log("victorian chair sitOn")
    }

}