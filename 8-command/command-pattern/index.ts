import { Button } from './Buttton';
import { CustomerService } from './CustomerService';
import { AddCustomerCommand } from './AddCustomerCommand';

const service = new CustomerService(); // Receiver
const addCustomer = new AddCustomerCommand(service); // Command
const button = new Button(addCustomer); // Sender
button.click();
