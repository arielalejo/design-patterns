import { ICommand } from './ICommand';

export class Button {
    private _command: ICommand;
    constructor(command: ICommand) {
        this._command = command;
    }
    click() {
        this._command.execute();
    }
}
