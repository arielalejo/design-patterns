import { ICommand } from './ICommand';
import { CustomerService } from './CustomerService';

export class AddCustomerCommand implements ICommand {
    private _service: CustomerService;

    constructor(service: CustomerService) {
        this._service = service;
    }

    execute(): void {
        this._service.addCustomer();
    }
}
