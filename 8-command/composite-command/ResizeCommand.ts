import { ICommand } from './ICommand';

export class ResizeCommand implements ICommand {
    execute(): void {
        console.log('resize image...');
    }
}
