import { CompositeCommand } from './CompositeCommand';
import { BlackandWhiteCommand } from './BlackandWthiteCommand';
import { ResizeCommand } from './ResizeCommand';

const compositeCommand = new CompositeCommand();
compositeCommand.addCommand(new ResizeCommand());
compositeCommand.addCommand(new BlackandWhiteCommand());
compositeCommand.execute();
