import { ICommand } from './ICommand';

export class CompositeCommand implements ICommand {
    private _commands: ICommand[] = [];

    addCommand(command: ICommand) {
        this._commands.push(command);
    }

    execute(): void {
        for (var command of this._commands) {
            command.execute();
        }
    }
}
