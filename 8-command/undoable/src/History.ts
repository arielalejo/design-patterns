import { IUndoableCommand } from './IUndoableCommand';

export class History {
    private _commands: IUndoableCommand[] = [];

    push(command: IUndoableCommand) {
        this._commands.push(command);
    }

    pop(): IUndoableCommand {
        const command = <IUndoableCommand>this._commands.pop();
        return command;
    }

    size(): Number {
        return this._commands.length;
    }
}
