export class HtmlDocument {
    private _content: String;

    makeBold = () => {
        this._content = `<b>${this._content}<b>`;
    };

    get content() {
        return this._content;
    }

    set content(value: String) {
        this._content = value;
    }
}
