import { HtmlDocument } from './HtmlDocument';
import { BoldCommand } from './BoldCommand';
import { History } from './History';
import { UndoCommand } from './UndoCommand';

const history = new History();
const htmlDocument = new HtmlDocument();
const boldCommand = new BoldCommand(htmlDocument, history);
const undoCommand = new UndoCommand(history);

htmlDocument.content = 'hello world';

/* execute */
boldCommand.execute();
boldCommand.execute();
boldCommand.execute();
console.log(htmlDocument.content);

/* unexecute */
undoCommand.execute();
console.log(htmlDocument.content);
undoCommand.execute();
console.log(htmlDocument.content);

/* ****************************************** */
/*setting another content in the htmlDocument*/
htmlDocument.content = 'good bye blue sky';

boldCommand.execute();
console.log(htmlDocument.content);

undoCommand.execute();
console.log(htmlDocument.content);

undoCommand.execute();
console.log(htmlDocument.content);

undoCommand.execute();
console.log(htmlDocument.content);
