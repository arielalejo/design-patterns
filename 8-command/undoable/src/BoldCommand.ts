import { IUndoableCommand } from './IUndoableCommand';
import { HtmlDocument } from './HtmlDocument';
import { History } from './History';

export class BoldCommand implements IUndoableCommand {
    private _prevContent: String;
    private _document: HtmlDocument;
    private _history: History;

    constructor(doc: HtmlDocument, hist: History) {
        this._document = doc;
        this._history = hist;
    }

    execute(): void {
        // const clone = Object.assign(Object.create(BoldCommand.prototype), this);
        // clone._prevContent = this._document.content;
        
        this._prevContent = this._document.content;
        this._document.makeBold();
        this._history.push(this);
    }

    unexecute(): void {
        this._document.content = this._prevContent;
    }
}
