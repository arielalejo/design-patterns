import { ICommand } from './ICommand';
import { History } from './History';

export class UndoCommand implements ICommand {
    private _history: History;

    constructor(hist: History) {
        this._history = hist;
    }

    execute(): void {        
        if (this._history.size() > 0) {
            // console.log(this._history.size());
            const lastCommand = this._history.pop();
            lastCommand.unexecute();
        }
        // console.log(this._history.size());
    }
}
