import { ICommand } from './ICommand';

export interface IUndoableCommand extends ICommand {
    unexecute(): void;
}
