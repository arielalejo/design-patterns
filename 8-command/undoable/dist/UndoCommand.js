"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var UndoCommand = /** @class */ (function () {
    function UndoCommand(hist) {
        this._history = hist;
    }
    UndoCommand.prototype.execute = function () {
        console.log(this._history.size());
        if (this._history.size() > 0) {
            var lastCommand = this._history.pop();
            lastCommand.unexecute();
        }
        console.log(this._history.size());
    };
    return UndoCommand;
}());
exports.UndoCommand = UndoCommand;
//# sourceMappingURL=UndoCommand.js.map