"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var HtmlDocument = /** @class */ (function () {
    function HtmlDocument() {
        var _this = this;
        this.makeBold = function () {
            _this._content = "<b>" + _this._content + "<b>";
        };
    }
    Object.defineProperty(HtmlDocument.prototype, "content", {
        get: function () {
            return this._content;
        },
        set: function (value) {
            this._content = value;
        },
        enumerable: true,
        configurable: true
    });
    return HtmlDocument;
}());
exports.HtmlDocument = HtmlDocument;
//# sourceMappingURL=HtmlDocument.js.map