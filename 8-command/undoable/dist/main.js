"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var HtmlDocument_1 = require("./HtmlDocument");
var BoldCommand_1 = require("./BoldCommand");
var History_1 = require("./History");
var UndoCommand_1 = require("./UndoCommand");
var history = new History_1.History();
var htmlDocument = new HtmlDocument_1.HtmlDocument();
var boldCommand = new BoldCommand_1.BoldCommand(htmlDocument, history);
var undoCommand = new UndoCommand_1.UndoCommand(history);
htmlDocument.content = 'hello world';
/* execute */
boldCommand.execute();
boldCommand.execute();
boldCommand.execute();
console.log(htmlDocument.content);
/* unexecute */
undoCommand.execute();
console.log(htmlDocument.content);
undoCommand.execute();
console.log(htmlDocument.content);
undoCommand.execute();
console.log(htmlDocument.content);
/*setting another content in the htmlDocument*/
// htmlDocument.content = 'good bye blue sky';
//boldCommand.execute();
//console.log(htmlDocument.content);
//# sourceMappingURL=main.js.map