"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var HtmlDocument_1 = require("./HtmlDocument");
var BoldCommand_1 = require("./BoldCommand");
var History_1 = require("./History");
var UndoCommand_1 = require("./UndoCommand");
var htmlDocument = new HtmlDocument_1.HtmlDocument();
htmlDocument.content = 'hello world';
var history = new History_1.History();
var boldCommand = new BoldCommand_1.BoldCommand(htmlDocument);
// boldCommand.document = htmlDocument;
boldCommand.history = history;
/*   methods are propense to be changed -> warning*/
// boldCommand.document.makeBold  = ()=>{};
// htmlDocument.makeBold = () => {};
/*  */
// execute and see the result
boldCommand.execute();
boldCommand.execute();
boldCommand.execute();
console.log(htmlDocument.content);
// unexecute
var undoCommand = new UndoCommand_1.UndoCommand();
undoCommand.history = history;
undoCommand.execute();
undoCommand.execute();
// undoCommand.execute();
// boldCommand.unexecute();
// boldCommand.unexecute();
console.log(htmlDocument.content);
/*setting another content in the htmlDocument*/
// htmlDocument.content = 'good bye blue sky';
//boldCommand.execute();
//console.log(htmlDocument.content);
