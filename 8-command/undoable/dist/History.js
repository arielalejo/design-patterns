"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var History = /** @class */ (function () {
    function History() {
        this._commands = [];
    }
    History.prototype.push = function (command) {
        this._commands.push(command);
    };
    History.prototype.pop = function () {
        var command = this._commands.pop();
        return command;
    };
    History.prototype.size = function () {
        return this._commands.length;
    };
    return History;
}());
exports.History = History;
//# sourceMappingURL=History.js.map