"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var BoldCommand = /** @class */ (function () {
    function BoldCommand(doc, hist) {
        this._document = doc;
        this._history = hist;
    }
    BoldCommand.prototype.execute = function () {
        this._prevContent = this._document.content;
        this._document.makeBold();
        this._history.push(this);
    };
    BoldCommand.prototype.unexecute = function () {
        this._document.content = this._prevContent;
    };
    return BoldCommand;
}());
exports.BoldCommand = BoldCommand;
//# sourceMappingURL=BoldCommand.js.map