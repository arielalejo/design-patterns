package observer;

public class App {
    public static void main(String[] args) throws Exception {
        var dataSource = new DataSource();
        dataSource.addObserver(new Chart());
        dataSource.addObserver(new SpreasSheet());

        dataSource.setValue("new value");
    }
}
