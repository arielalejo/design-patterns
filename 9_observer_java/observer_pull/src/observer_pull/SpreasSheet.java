package observer_pull;

public class SpreasSheet implements IObserver {
    private DataSource _subject;

    public SpreasSheet(DataSource subjet) {
        this._subject = subjet;
    }

    @Override
    public void update() {

        var notifiedValue = this._subject.getValue();
        System.out.println("updating spreadsheeet mechanism..." + notifiedValue);
    }

}
