package observer_pull;

public class Chart implements IObserver {
    private DataSource _subject;

    public Chart(DataSource subject) {
        this._subject = subject;
    }

    @Override
    public void update() {

        var notifiedValue = this._subject.getValue();
        System.out.println("updating chart mechanism..." + notifiedValue);
    }

}
