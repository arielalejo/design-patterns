package observer_pull;

public class App {
    public static void main(String[] args) throws Exception {
        var dataSource = new DataSource();
        var chart1 = new Chart(dataSource);
        var spreadsheet = new SpreasSheet(dataSource);

        dataSource.addObserver(chart1);
        dataSource.addObserver(spreadsheet);

        dataSource.setValue("NEW VALUE");

        // dataSource.removeObserver(chart1);

        dataSource.setValue("modified value");
    }
}
