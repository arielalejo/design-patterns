package observer_pull;

import java.util.ArrayList;
import java.util.List;

public class Subject {
    private List<IObserver> _observers = new ArrayList<>();

    public void addObserver(IObserver observer) {
        this._observers.add(observer);
    }

    public void removeObserver(IObserver observer) {
        this._observers.remove(observer);
    }

    public void notifyObsevers() {
        for (var observer : this._observers)
            observer.update();
    }
}
