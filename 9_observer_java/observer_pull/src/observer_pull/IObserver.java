package observer_pull;

public interface IObserver {
    public void update();
}
