package observer_push;

public class Chart implements IObserver {

    @Override
    public void update(String value) {
        System.out.println("updating chart mechanism..." + value);

    }

}
