package observer_push;

public class App {
    public static void main(String[] args) throws Exception {
        var dataSource = new DataSource();
        var chart1 = new Chart();
        var chart2 = new Chart();
        var spreadsheet = new SpreasSheet();

        dataSource.addObserver(chart1);
        dataSource.addObserver(spreadsheet);
        dataSource.addObserver(chart2);

        dataSource.setValue("NEW VALUE");

        dataSource.removeObserver(chart2);

        dataSource.setValue("MODIFIED VALUE");
    }
}
