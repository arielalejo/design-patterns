package observer_push;

public interface IObserver {
    public void update(String value);
}
