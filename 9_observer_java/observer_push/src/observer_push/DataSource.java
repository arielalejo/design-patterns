package observer_push;

public class DataSource extends Subject {
    private String _value;

    public String getValue() {
        return _value;
    }

    public void setValue(String value) {
        this._value = value;
        notifyObsevers(this._value);
    }

}
