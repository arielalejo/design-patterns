import { IState } from "./IState";
import { StoppedState } from "./StoppedState";

export class StopWatch{
    private _state: IState = new StoppedState(this);

    set state(state: IState) {
        this._state = state;
    }

    click(){
        this._state.click();
    }
}