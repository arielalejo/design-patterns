import { IState } from './IState';
import { StopWatch } from './StopWatch';
import { StoppedState } from './StoppedState';

export class RunningState implements IState {
    private _stopWatch: StopWatch;

    constructor(stopwatch: StopWatch) {
        this._stopWatch = stopwatch;
    }

    click(): void {
        /*  - if the state of stowatch is in RunningState and we click it, 
          then: the stopwatch is stopped, so we set the state of stopwatch 
          to the StoppedState 
        - print a message for the now current state -> stopped */
        this._stopWatch.state = new StoppedState(this._stopWatch);
        console.log('stopped');
    }
}
