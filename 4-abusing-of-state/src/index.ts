import { StopWatchNormal } from './StopWatchNormal';
import { StopWatch } from './StopWatch';

// let sw = new StopWatchNormal();  // both should work
const sw = new StopWatch(); // both should work
sw.click();
sw.click();
sw.click();
