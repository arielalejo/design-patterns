import { IState } from "./IState";
import { StopWatch } from "./StopWatch";
import { RunningState } from "./RunningState";

export class StoppedState implements IState{
    private _stopWatch: StopWatch;
    
    constructor(stopwatch: StopWatch) {
        this._stopWatch = stopwatch;
    }

    click(): void {
        /* - if the state of stowatch is in stoppedState and we click it, 
           then: the stopwatch is running, so we set the state of stopwatch 
           to the RunningState 
           - print a message for the now current state -> running*/        
        this._stopWatch.state = new RunningState(this._stopWatch);
        console.log("running");
        
    }

}