export class StopWatchNormal {
    private isRunning: boolean;

    click() {
        if (this.isRunning) {
            this.isRunning = false;
            console.log("stopped");
        }
        else {
            this.isRunning = true;
            console.log("running");
        }

    }
}