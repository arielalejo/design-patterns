import { UIControl } from "./UIControl";

export class ListBox extends UIControl{
    private _selection: string;
    // private _observers: IObserver[];        

    // public attach(obs: IObserver):void{
    //     this._observers.push(obs);
    // }

    // protected notify(): void{
    //     for(let observer of this._observers){
    //         observer.update()
    //     }
    // }
    get selection(): string{
        return this._selection;
    }

    set selection(value: string){
        this._selection = value;
        this.notify();
    }
}
