import { IObserver } from "./IObserver";

export abstract class UIControl{
    private _observers: IObserver[] = [];        

    public attach(obs: IObserver):void{
        this._observers.push(obs);
    }

    protected notify(): void{
        for(let observer of this._observers){
            observer.update()
        }
    }
}
