import { ListBox } from './ListBox';
import { TextBox } from './TextBox';
import { Button } from './Button';
import { IObserver } from './IObserver';

export class ArticlesDialogBox {
    private articlesListBox: ListBox = new ListBox();
    private titleTextBox: TextBox = new TextBox();
    private saveButton: Button = new Button();
    
    constructor(){
        this.articlesListBox.attach(new this.ListBoxObserver(this));
        this.titleTextBox.attach(new this.TextObserver(this));
    }

    public simulate(): void {
        this.articlesListBox.selection = 'Article1';
        this.titleTextBox.content = '';
        console.log('TextBox: ', this.titleTextBox.content);
        console.log('Button: ', this.saveButton.isEnabled);
    }

    // ****** inner classes ***************
    private ListBoxObserver = class implements IObserver{
        constructor(private superThis: ArticlesDialogBox){}
        
        update():void { this.superThis.articleSelected(); }
    }

    private TextObserver = class implements IObserver{
        constructor(private superThis: ArticlesDialogBox){}

        update(): void { this.superThis.textBoxChanged(); }
    }
    
    // **** specific update methods for specific Observers ******************
    private articleSelected():void {
        this.titleTextBox.content = this.articlesListBox.selection;
        this.saveButton.isEnabled = true;
    }

    private textBoxChanged() :void{
        const content = this.titleTextBox.content;
        const isEmpty = content == null || content == '';
        this.saveButton.isEnabled = !isEmpty;
    }
}
