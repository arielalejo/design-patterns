import { EditorState } from "./EditorState";

export class History {
    private _states: EditorState[] = [];

    push(state: EditorState) {
        this._states.push(state);
    }

    pop(): EditorState {
        let lastIndex = this._states.length -1;
        let lastState = this._states[lastIndex];
        this._states.pop();
        return lastState;
    }

}