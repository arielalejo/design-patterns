import { EditorState } from "./EditorState";

export class Editor{
    private _content: string;

    constructor(content: string = "") {
        this._content = content;
    }

    get content(): string{
        return this._content;
    }  
    
    set content(content: string) {
        this._content = content;
    }

    createState(): EditorState {
        return new EditorState(this._content);
    }

    restoreState(state: EditorState) {
        this._content = state.content;
    }
}