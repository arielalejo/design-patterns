import { Editor } from "./Editor";
import { History } from "./History";

// 1. instanciate Editor and History classes
let editor = new Editor();
let history = new History();

// 2. set a content in the editor; get the current state; store that state in the history
editor.content = "a";
let stateToSave = editor.createState();
history.push(stateToSave);

// 2. set other content in the editor; get the current state; store that state in the history
editor.content = "b";
stateToSave = editor.createState();
history.push(stateToSave);

// 3. set other contents, but not save those states
editor.content = "c";
editor.content = "d";

// 4. recovery the last state ("b") in the editor object 
let lastState = history.pop();
editor.restoreState(lastState);

// RES: get the actual state of editor
console.log(editor.content);
