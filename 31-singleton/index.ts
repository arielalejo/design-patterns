class Singleton{
    public name:string;  // instance property
    private static instance: Singleton;

    private constructor(){
        this.name = "abc";
    }

    public static getInstance(){
        if (!Singleton.instance){
            Singleton.instance = new Singleton();
        }

        return Singleton.instance;
    }
}

// instantiate Class
const single = Singleton.getInstance();
/*
single.name // "abc"

new Singleton()    -> doesn't work!
Singleton.instance -> instance is private, will throw Error.
*/
