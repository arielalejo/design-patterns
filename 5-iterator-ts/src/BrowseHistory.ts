import { IBaseIterator } from './IBaseIterator';

export class BrowseHistory <T>{
    private _urls: T[] = [];

    push(url: T) {
        this._urls.push(url);
    }

    pop() {
        let lastIndex = this._urls.length;
        let lastUrl = this._urls[lastIndex];
        this._urls.pop();
        return lastUrl;
    }

    createListIterator() {
        return new this.ListIterator(this);
    }

    // inner class
    ListIterator = class implements IBaseIterator<T> {
        private _index: number = 0;
        private _history: BrowseHistory<T>;

        constructor(history: BrowseHistory<T>) {
            this._history = history;
        }

        current(): T {
            return this._history._urls[this._index];
        }

        next(): void {
            this._index++;
        }
        hasNext(): boolean {
            return this._index < this._history._urls.length;
        }
    };
}
