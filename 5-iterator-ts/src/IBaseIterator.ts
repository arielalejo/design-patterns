export interface IBaseIterator<T>{
    current(): T,
    next(): void,
    hasNext(): boolean
}