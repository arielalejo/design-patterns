import { BrowseHistory } from "./BrowseHistory";

let history = new BrowseHistory<number>();
history.push(1);
history.push(2);
history.push(3);

let iterator = history.createListIterator();

while(iterator.hasNext()){
    let url = iterator.current();
    console.log(url);
    iterator.next();
}


// let urls = history.urls;
// for (let i=0; i< urls.length; i++){
//     console.log(urls[i]);
// }