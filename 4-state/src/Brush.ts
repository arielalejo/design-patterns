import { ITool } from './ITool';

export class Brush implements ITool {
    mouseDown(): void {
        console.log('Draw a circle');
    }
    mouseUp(): void {
        console.log('Start drawing');
    }
}
