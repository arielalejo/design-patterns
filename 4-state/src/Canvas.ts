import { ITool } from "./ITool";

export class Canvas {
    private _currentTool: ITool;

    set currentTool(tool: ITool) {
        this._currentTool = tool;
    }

    mouseDown() {
        this._currentTool.mouseDown();
    }

    mouseUp() {
        this._currentTool.mouseUp();
    }
}
