import { Canvas } from "./Canvas";
import { SelectionTool } from "./SelectionTool";
import { Brush } from "./Brush";
import { EraseTool } from "./EraseTool";

// 1. instantiate the canvas
let canvas = new Canvas();

// 2. select an specific tool for the canvas
let currentTool = new EraseTool()  // or any tool
canvas.currentTool = currentTool;

// 3. execute the methods independently of the tool
canvas.mouseDown();
canvas.mouseUp();

