import { ComputerBuilder } from './ComputerBuilder';
import { Vendor } from './Vendor';

const builder = new ComputerBuilder();
const vendor = new Vendor(builder);

const gamerPc = vendor.constructGamerPc();
console.log(gamerPc);
