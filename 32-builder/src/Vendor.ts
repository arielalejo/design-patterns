import { IComputerBuilder } from './IComputerBuilder';

export class Vendor {
    private builder: IComputerBuilder;

    constructor(builder: IComputerBuilder) {
        this.builder = builder;
    }

    constructServer() {
        this.builder.buildMonitor('LG');
        this.builder.buildKeyboard('Dell');
        return this.builder.getResult()
    }

    constructGamerPc() {
        this.builder.buildMonitor('Samsumg');
        this.builder.buildKeyboard('Samsumg');
        this.builder.buildSpeaker('Gol');
        return this.builder.getResult()
    }
}
