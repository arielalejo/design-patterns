export class Computer{
    cpu: number;
    monitor:  string;
    keyboard: string;
    speakers: string;

    constructor(cpu: number){
        this.cpu = cpu;
    }
}