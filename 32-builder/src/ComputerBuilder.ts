import { Computer } from "./Computer";
import { IComputerBuilder } from "./IComputerBuilder";

export class ComputerBuilder implements IComputerBuilder{
    private computer: Computer;

    constructor(){
        this.computer = new Computer(8);
    }

    buildMonitor(monitor: string): IComputerBuilder {
        this.computer.monitor = monitor;
        return this;
    }

    buildKeyboard(keyboard: string): IComputerBuilder {
        this.computer.keyboard = keyboard;
        return this;
    }

    buildSpeaker(speakers: string): IComputerBuilder {
        this.computer.speakers = speakers;
        return this;        
    }

    getResult(): Computer {
        const computer = {...this.computer};
        this.computer = new Computer(computer.cpu);
        return computer;
        
    }

}