import { Computer } from "./Computer";

export interface IComputerBuilder{
    buildMonitor(monitor: string): IComputerBuilder,
    buildKeyboard(keyboard: string): IComputerBuilder,
    buildSpeaker(speakers: string): IComputerBuilder,
    getResult(): Computer,
}