import { ComputerBuilder } from '../src/ComputerBuilder';
import { Vendor } from '../src/Vendor';

describe('Build a computer', () => {
    it('should create a GamerPC', () => {
        const builder = new ComputerBuilder();
        const vendor = new Vendor(builder);

        const gamerPc = vendor.constructGamerPc();
        const expectedGamer = {
            cpu: 8,
            monitor: 'Samsumg',
            keyboard: 'Samsumg',
            speakers: 'Gol',
        };

        expect(gamerPc).toStrictEqual(expectedGamer)
    });
});
