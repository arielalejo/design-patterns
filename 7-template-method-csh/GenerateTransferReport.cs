using System;

namespace _7_template_method_csh
{
    class GenerateTransferReport : Task
    {
        // *********************************************************        
        // *** before implementing the pattern ****
        // private AuditTrail _audit;

        // public GenerateTransferReport(AuditTrail audit)
        // {
        //     this._audit = audit;
        // }

        // public void execute()
        // {
        //     System.Console.WriteLine("Generating Tranfer Report");
        //     this._audit.record();
        // }
        // *********************************************************

        public GenerateTransferReport(AuditTrail audit) : base(audit) { }

        protected override void doExecute()
        {
            System.Console.WriteLine("Generating Transfer Report");
        }
    }
}