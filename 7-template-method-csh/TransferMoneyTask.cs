using System;

namespace _7_template_method_csh
{
    class TransferMoneyTask : Task
    {
        // *********************************************************
        // *** before implementing the pattern...****
        // private AuditTrail _audit;

        // public TransferMoneyTask(AuditTrail audit)
        // {
        //     this._audit = audit;
        // }

        // public void execute()
        // {
        //     this._audit.record();
        //     System.Console.WriteLine("Tranferring money");
        // }
        // *********************************************************

        public TransferMoneyTask(AuditTrail audit) : base(audit) { }

        protected override void doExecute()
        {
            System.Console.WriteLine("Transferring money");
        }
    }
}