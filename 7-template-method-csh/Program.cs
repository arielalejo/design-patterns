﻿using System;

namespace _7_template_method_csh
{
    class Program
    {
        static void Main(string[] args)
        {                      
            var audit = new AuditTrail();
            
            var tranferMoney = new TransferMoneyTask(audit);
            tranferMoney.execute();

            var report = new GenerateTransferReport(audit);
            report.execute();            
        }
    }
}
