using System;

namespace _7_template_method_csh
{
    abstract class Task
    {
        private AuditTrail _audit;

        protected Task(AuditTrail audit)
        {   
            this._audit = audit;
        }

        public void execute(){        
            this._audit.record(); 
            this.doExecute();            
        }

        abstract protected void doExecute();
    }
}